//
//  ContentView.swift
//  messagespeaker
//
//  Created by floh on 22.11.19.
//  Copyright © 2019 floh. All rights reserved.
//


import SwiftUI
import AVFoundation


struct ContentView: View {
    struct Message : Identifiable {
        var id = UUID()
        var text : String
        var color : Color
    }

    let messages = [
        Message(text: "Ich will ins Bett", color: .red),
        Message(text: "Ich muss auf's Klo", color: .yellow),
        Message(text: "Ich will raus", color: .blue),
        Message(text: "Ich will rein", color: .green),
    ]

    let synthesizer = AVSpeechSynthesizer()

    var body: some   View {
        GeometryReader { geometry in
            VStack(spacing: 0) {
                ForEach(self.messages, id: \.id) {message in
                    Button(action: {
                        let utterance = AVSpeechUtterance(string: message.text)
                        utterance.voice = AVSpeechSynthesisVoice(language: "de-DE")
                        utterance.rate = 0.5

                        self.synthesizer.speak(utterance)
                    }) {
                        VStack {
                            Text(message.text)
                            .font(.system(size:40))
                            .frame(width: geometry.size.width, height: geometry.size.height / 4)
                            .foregroundColor(.primary)
                            .background(message.color)
                        }
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
                .previewDevice("iPhone 11 Pro")
            ContentView()
                .previewDevice("iPhone 7")
        }
    }
}
